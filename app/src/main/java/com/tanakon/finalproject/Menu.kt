package com.tanakon.finalproject

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController



class Menu : Fragment() {
    private var binding: FragmentStartBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val menu = FragmentStartBinding.inflate(inflater, container, false)
        binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.apply {
            // Set up the button click listeners
            menu = this@Menu

        }
    }
    fun gotoDetail() {
        Toast.makeText(activity, "detail", Toast.LENGTH_SHORT).show()
        findNavController().navigate(R.id.action_munu_to_detail)
    }
    fun gotoPayment() {
        Toast.makeText(activity, "payment", Toast.LENGTH_SHORT).show()
        findNavController().navigate(R.id.action_munu_to_payment)
    }
}