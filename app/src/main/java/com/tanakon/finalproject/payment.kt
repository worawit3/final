package com.tanakon.finalproject

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController

class payment: Fragment()  {
    private var binding: FragmentStartBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val menu = FragmentStartBinding.inflate(inflater, container, false)
        binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.apply {
            // Set up the button click listeners
            payments = this@payment

        }
    }
    fun backMenu() {
        Toast.makeText(activity, "Back", Toast.LENGTH_SHORT).show()
        findNavController().navigate(R.id.action_back_to_menu)
    }
    fun backPayment() {
        Toast.makeText(activity, "payment", Toast.LENGTH_SHORT).show()
        findNavController().navigate(R.id.action_payment_to_menu)
    }
}